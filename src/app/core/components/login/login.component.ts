import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const body: HTMLElement = document.body;
    body.classList.add('initial-background');
  }

  ngOnDestroy(): void {
    const body: HTMLElement = document.body;
    body.classList.remove('initial-background');
  }

  onSubmit() {
    if(this.loginForm.valid) {
      this.authService.login({
        email: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value,
      }).then(
        (data) => {
          this.router.navigate(['/institution/add-facility']);

        }
      )
    }
  }

}
