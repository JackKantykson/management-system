import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registerForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const body: HTMLElement = document.body;
    body.classList.add('initial-background');
  }

  ngOnDestroy(): void {
    const body: HTMLElement = document.body;
    body.classList.remove('initial-background');
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.authService.register({
        email: this.registerForm.get('email').value,
        password: this.registerForm.get('password').value,
      }).then(
        (data: firebase.auth.UserCredential) => {
          this.router.navigate(['/institution/first-login']);
        }
      )

    }
  }

}

