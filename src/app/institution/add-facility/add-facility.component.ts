import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { CITIES, DAYS, FACILITY_TYPES, HOURS } from 'src/app/shared/constants/facility.constants';
import { FacilityInterface, StartEndInterface } from 'src/app/shared/interfaces/facility.interface';

@Component({
  selector: 'app-add-facility',
  templateUrl: './add-facility.component.html',
  styleUrls: ['./add-facility.component.scss']
})
export class AddFacilityComponent implements OnInit {
  public daysList: string[] = DAYS;
  public facilitySportTypes: string[] = FACILITY_TYPES;
  public hours: string[] = HOURS;
  public cities: string[] = CITIES;
  public addFacilityForm: FormGroup = this.createForm();
  public facilities: AngularFireList<FacilityInterface>;
  public userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFireDatabase,
    private authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.userId = this.authService.getCurrentUser().uid;
    this.facilities = this.db.list(`/facilities/${this.userId}`);
  }

  onSubmit(): void {
    const formData: FacilityInterface = this.prepareData();
    this.facilities.push(formData);
    this.router.navigate(['/institution/dashboard']);
  }

  onCancel(): void {
    this.router.navigate(['/institution/dashboard']);
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.required],
      city: ['', Validators.required],
      comment: '',
      street: ['', Validators.required],
      postalCode: ['', Validators.required],
      imageUrl: ['', Validators.required],
      facilitySportType: ['', Validators.required],
      Monday: this.createDayFormGroup(),
      Tuesday: this.createDayFormGroup(),
      Wednesday: this.createDayFormGroup(),
      Thursday: this.createDayFormGroup(),
      Friday: this.createDayFormGroup(),
      Saturday: this.createDayFormGroup(),
      Sunday: this.createDayFormGroup(),
    });
  }

  private createDayFormGroup(): FormGroup {
    return this.formBuilder.group({
      isAvailable: false,
      start: '',
      end: ''
    })
  }

  private prepareData(): FacilityInterface {
    return {
      name: this.addFacilityForm.get('name').value,
      city: this.addFacilityForm.get('city').value,
      comment: this.addFacilityForm.get('comment').value,
      street: this.addFacilityForm.get('street').value,
      postalCode: this.addFacilityForm.get('postalCode').value,
      imageUrl: this.addFacilityForm.get('imageUrl').value,
      facilitySportType: this.addFacilityForm.get('facilitySportType').value,
      Monday: this.getAvailability('Monday'),
      Tuesday: this.getAvailability('Tuesday'),
      Wednesday: this.getAvailability('Wednesday'),
      Thursday: this.getAvailability('Thursday'),
      Friday: this.getAvailability('Friday'),
      Saturday: this.getAvailability('Saturday'),
      Sunday: this.getAvailability('Sunday'),
    }
  }

  private getAvailability(day: string): StartEndInterface | null {
    const isAvailable: boolean = this.addFacilityForm.get(`${day}.isAvailable`).value;

    return isAvailable
      ? {
        start: this.addFacilityForm.get(`${day}.start`).value,
        end: this.addFacilityForm.get(`${day}.end`).value,
      }
      : null
  }
}
