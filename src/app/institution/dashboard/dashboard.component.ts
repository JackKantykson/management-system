import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/auth/auth.service';
import { ConfirmDialogComponent, ConfirmDialogModel } from 'src/app/shared/components/confirm-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private userId: string;
  public facilities$: Observable<unknown[]>;
  public dbFacilities: AngularFireList<any>;

  public constructor(
    private formBuilder: FormBuilder,
    private db: AngularFireDatabase,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    this.userId = this.authService.getCurrentUser().uid;
    this.dbFacilities = this.db.list(`/facilities/${this.userId}`);
    this.facilities$ = this.dbFacilities.snapshotChanges().pipe(
      map((changes: any) => changes.map(c => ({ key: c.payload.key, ...c.payload.val()})))
    )
  }

  public addNew(): void {
    this.router.navigate(['/institution/add-facility']);
  }

  public onDelete(facility: any): void {
    const message = `Are you sure you want to remove facility?`;

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
      if(dialogResult) {
        this.dbFacilities.remove(facility.key);
      }
    });
  }
}
