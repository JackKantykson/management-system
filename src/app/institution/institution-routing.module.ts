import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddFacilityComponent } from 'src/app/institution/add-facility/add-facility.component';
import { FirstLoginComponent } from 'src/app/institution/first-login/first-login.component';
import { DashboardComponent } from 'src/app/institution/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    pathMatch: 'full',
  },
  {
    path: 'add-facility',
    component: AddFacilityComponent,
    pathMatch: 'full',
  },
  {
    path: 'first-login',
    component: FirstLoginComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitutionRoutingModule{ }
