import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstitutionRoutingModule } from 'src/app/institution/institution-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddFacilityComponent } from './add-facility/add-facility.component';
import { FirstLoginComponent } from './first-login/first-login.component';


@NgModule({
  declarations: [
    DashboardComponent,
    AddFacilityComponent,
    FirstLoginComponent,
  ],
  imports: [
    CommonModule,
    InstitutionRoutingModule,
    SharedModule
  ]
})
export class InstitutionModule { }
