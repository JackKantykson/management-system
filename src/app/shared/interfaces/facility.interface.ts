export interface FacilityInterface {
  name: string;
  city: string,
  comment: string,
  street: string,
  postalCode: string,
  imageUrl: string,
  facilitySportType: string,
  Monday: StartEndInterface | null,
  Tuesday: StartEndInterface | null,
  Wednesday: StartEndInterface | null,
  Thursday: StartEndInterface | null,
  Friday: StartEndInterface | null,
  Saturday: StartEndInterface | null,
  Sunday: StartEndInterface | null,
}

export interface StartEndInterface {
  start: string,
  end: string,
}




