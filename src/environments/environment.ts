// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB37bruAGL9wigdvdlbmyVMgsfAlRjmJzQ",
    authDomain: "management-system-1c107.firebaseapp.com",
    databaseURL: "https://management-system-1c107.firebaseio.com",
    projectId: "management-system-1c107",
    storageBucket: "management-system-1c107.appspot.com",
    messagingSenderId: "631846222411",
    appId: "1:631846222411:web:80c96e09119618d04c6bce",
    measurementId: "G-LDMWT2TVFT"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
